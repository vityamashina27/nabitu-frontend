/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    defaultLocale: 'id'
}

module.exports = nextConfig
