import cookie from "cookie";

export const getAccessToken = async () => {
	const cookies = cookie.parse(document.cookie);
	const refreshToken = cookies["refresh-token"];
	const endpoint = `${process.env.NEXT_PUBLIC_API_BASE_URL}/session`;
	const getToken = await fetch(endpoint, {
		method: "PUT",
		body: JSON.stringify({ token: refreshToken }),
		headers: {
			"Content-Type": "application/json",
		},
	});

	let { ok, msg, err, data } = await getToken.json();

	if (ok) {
		document.cookie = `access-token=${data.access_token}; path=/; expires=Fri, 31 Dec 9999 23:59:59 GMT;`;
	}

	return { ok, msg, err, data };
};

export const accessToken = () => {
	const cookies = cookie.parse(document.cookie);
	return cookies["access-token"];
};

export const refreshToken = () => {
	const cookies = cookie.parse(document.cookie);
	return cookies["refresh-token"];
};

export const tokenHandler = async (callback: Promise<any>) => {
	const getNewAccessToken = await getAccessToken();
	if (getNewAccessToken.ok) {
		const reFetchingAPI = await callback;
        return reFetchingAPI
	} else {
		console.log(getNewAccessToken);
        return getNewAccessToken
	}
};

export const destroyCookie = () => {
	document.cookie = `access-token=null; path=/; expires=Fri, 31 Dec 1000 23:59:59 GMT;`;
	document.cookie = `refresh-token=null; path=/; expires=Fri, 31 Dec 1000 23:59:59 GMT;`;
}
