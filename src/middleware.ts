import { NextRequest, NextResponse } from "next/server";
import { cookies } from "next/headers";

const getAccessToken = async (refreshToken: string) => {
	const fetchRefreshToken = await fetch(
		`${process.env.NEXT_PUBLIC_API_BASE_URL}/session`,
		{
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				token: refreshToken,
			}),
		}
	);

	const { ok, msg, data } = await fetchRefreshToken.json();
	if (!ok) {
		console.error("[FETCH_TOKEN] FAILED TO GET ACCESS TOKEN");
		console.error(`[FETCH_TOKEN] MESSAGE : ${msg}`);
		return false;
	}

	console.error("[FETCH_TOKEN] SUCCESS TO GET ACCESS TOKEN");
	console.error(`[FETCH_TOKEN] NEW TOKEN : ${data.access_token}`);
	return true;
};

export default async function middleware(request: NextRequest) {

    const refreshToken: string = request.cookies.get('refresh-token')?.value ?? '';

	const isLoggedIn = await getAccessToken(refreshToken);

	if (!isLoggedIn) {
		return NextResponse.redirect(new URL("/login", request.url));
	}

	return NextResponse.next()
}

export const config = {
	matcher: ["/", "/reminders/:path*"],
};
