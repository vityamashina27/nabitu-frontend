import FloatingButton from "@/components/atoms/FloatingButton";
import LogoutButton from "@/components/atoms/LogoutButton";
import ReminderList from "@/components/molecules/ReminderList";
import { Heading } from "@chakra-ui/react";

export default function Home({ searchParams }: { searchParams: any }) {
	return (
		<main className="min-h-screen relative">
			<div className="flex justify-between">
				<Heading size={"lg"}> Reminder Lists </Heading>
				<LogoutButton />
			</div>
			<ReminderList q={searchParams} />
			<FloatingButton />
		</main>
	);
}
