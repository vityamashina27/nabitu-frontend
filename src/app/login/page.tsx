"use client";

import { useToast } from "@chakra-ui/react";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { useForm } from "react-hook-form";

const page = () => {
	const router = useRouter();
	const toast = useToast();
	useEffect(() => {
		toast({
			title: "You must login first.",
			position: "top",
		});
	}, []);

	const { register, handleSubmit, setValue } = useForm();

	const onSubmit = async (formData: any) => {
		if (formData.email == "" || formData.password == "") {
			return toast({
				status: "warning",
				title: "Please enter email and password",
			});
		}

		const endpoint = `/session`;
		const login = await fetch(
			`${process.env.NEXT_PUBLIC_API_BASE_URL}${endpoint}`,
			{
				method: 'post',
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(formData),
			}
		).catch((err) => {})

		const { ok, data, msg } = await login.json();

        if(!ok) return toast({
            status: 'error',
            title: msg
        })

        const access_token = data.access_token
        const refresh_token = data.refresh_token

        document.cookie = `access-token=${access_token}; path=/; expires=Fri, 31 Dec 9999 23:59:59 GMT;`;
        document.cookie = `refresh-token=${refresh_token}; path=/; expires=Fri, 31 Dec 9999 23:59:59 GMT;`;

		router.push('/')
	};

	return (
		<div className="w-screen h-screen flex items-center justify-center">
			<div className="rounded-md shadow-lg w-1/2 min-h-[10vh] p-10 flex flex-col gap-4">
				<h1 className="text-lg">Login</h1>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className="flex flex-col gap-4"
				>
					<input
						{...register("email")}
						type="email"
						className="shadow-sm appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
						placeholder="Your email"
					/>
					<input
						{...register("password")}
						className="shadow-sm appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
						placeholder="Your password"
						type="password"
					/>
					<button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
						Login
					</button>
				</form>
			</div>
		</div>
	);
};

export default page;
