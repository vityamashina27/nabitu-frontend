import BackToHome from "@/components/molecules/BackToHome";
import { Heading } from "@chakra-ui/react";
import FormReminder from "@/components/molecules/FormReminder";
export default function page() {
	return (
		<div>
			<BackToHome />
			<Heading size="md"> Add Reminder </Heading>
			<FormReminder />
		</div>
	);
}
