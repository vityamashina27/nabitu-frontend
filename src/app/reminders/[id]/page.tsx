import BackToHome from "@/components/molecules/BackToHome";
import ReminderDetailCard from "@/components/molecules/ReminderDetailCard";
import ReminderDetailForm from "@/components/organisms/ReminderDetailForm";

export default function page({ params }: { params: { id: number } }) {
	return (
		<div>
			<BackToHome />
			<div className="flex gap-4 justify-between">
				<ReminderDetailForm id={params.id}/>
				<ReminderDetailCard id={params.id}/>
			</div>
		</div>
	);
}
