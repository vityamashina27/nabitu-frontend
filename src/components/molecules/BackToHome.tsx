import { Button, Heading } from "@chakra-ui/react";
import { AiOutlineArrowLeft } from "@react-icons/all-files/ai/AiOutlineArrowLeft";
import Link from "next/link";

export default function BackToHome() {
	return (
		<Link href={"/"}>
			<Heading size={"md"} className="mb-4">
				<Button
					leftIcon={<AiOutlineArrowLeft />}
					colorScheme="teal"
					variant={"solid"}
				>
					Back to home
				</Button>
			</Heading>
		</Link>
	);
}
