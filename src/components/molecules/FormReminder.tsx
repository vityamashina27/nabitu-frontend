"use client";
import { Button, useToast } from "@chakra-ui/react";
import InputGroup from "./InputGroup";
import { FaSave } from "@react-icons/all-files/fa/FaSave";
import { useForm } from "react-hook-form";
import { useRouter } from "next/navigation";
import dayjs from "dayjs";
import { useEffect, useState } from "react";
import { accessToken, tokenHandler } from "@/services/AuthService";

type Reminder = {
	id?: number;
	title: string;
	description: string;
	event_at: number;
	remind_at: number;
};

type SaveReminder = {
	endpoint: string;
	type: string;
	reminder: object;
};

const saveReminder = async (request: SaveReminder) => {
	const storeReminder = await fetch(
		`${process.env.NEXT_PUBLIC_API_BASE_URL}${request.endpoint}`,
		{
			method: request.type == "add" ? "POST" : "PUT",
			headers: {
				"Content-Type": "application/json",
				authorization: "Bearer " + accessToken(),
			},
			body: JSON.stringify(request.reminder),
		}
	);

	return await storeReminder.json();
};
export default function FormReminder({
	type = "add",
	reminderData,
}: {
	type?: "add" | "edit";
	reminderData?: Reminder;
}) {
	const { register, handleSubmit, setValue } = useForm();
	const [reminder, setReminder] = useState<Reminder>();

	const toast = useToast();
	const router = useRouter();

	const onSubmit = async (formData: any) => {
		const reminderRequest: Reminder = formData;
		reminderRequest.remind_at = dayjs(reminderRequest.remind_at).unix();
		reminderRequest.event_at = dayjs(reminderRequest.event_at).unix();

		const endpoint =
			type == "add" ? "/reminders" : `/reminders/${reminderData?.id}`;
		console.log("request", JSON.stringify(reminderRequest));

		const { ok, data, err, msg } = await saveReminder({
			endpoint: endpoint,
			type: type,
			reminder: reminderRequest,
		});

		if (err === "ERR_INVALID_ACCESS_TOKEN") {
			await tokenHandler(
				saveReminder({
					endpoint: endpoint,
					type: type,
					reminder: reminderRequest,
				})
			);
		}

		{
			ok
				? toast({
						status: "success",
						title: `Reminder ${
							type == "add" ? "added" : "updated"
						}`,
						onCloseComplete() {
							router.push("/");
						},
						duration: 500,
				  })
				: toast({
						status: "warning",
						title: msg,
				  });
		}
	};

	useEffect(() => {
		setReminder(reminderData);
		if (reminder) {
			setValue("title", reminder.title);
			setValue("description", reminder.description);
			setValue(
				"event_at",
				dayjs.unix(reminder?.event_at).format("YYYY-MM-DD HH:mm:ss")
			);
			setValue(
				"remind_at",
				dayjs.unix(reminder?.remind_at).format("YYYY-MM-DD HH:mm:ss")
			);
		}
	}, [reminderData]);

	return (
		<form
			onSubmit={handleSubmit(onSubmit)}
			className="mt-4 flex flex-col gap-4"
		>
			<InputGroup
				inputRegister={{
					...register("title"),
				}}
				title="Title"
				placeholder="input your reminder here"
			/>
			<InputGroup
				inputRegister={{ ...register("description") }}
				title="Description"
				placeholder="input description here"
			/>
			<InputGroup
				inputRegister={{ ...register("event_at") }}
				type="datetime-local"
				title="Event start"
				id="event_at"
				placeholder="input event start here"
				value={
					!reminderData ? dayjs().format("YYYY-MM-DD HH:mm:ss") : ""
				}
			/>
			<InputGroup
				inputRegister={{
					...register("remind_at"),
				}}
				type="datetime-local"
				title="Remind at"
				id="remind_at"
				placeholder="input remind at here"
				value={
					!reminderData ? dayjs().format("YYYY-MM-DD HH:mm:ss") : ""
				}
			/>
			<Button
				type="submit"
				leftIcon={<FaSave />}
				colorScheme="teal"
				variant={"solid"}
			>
				{type == "add" ? "Save" : "Update"}
			</Button>
		</form>
	);
}
