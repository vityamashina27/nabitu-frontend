import {
	FormControl,
	FormHelperText,
	FormLabel,
	Input,
} from "@chakra-ui/react";

type Input = {
	id?: string;
	title: string;
	type?: string;
	message?: string;
	placeholder?: string;
	value?: string;
	callback?: any
	inputRegister?: any,
	defaultValue?: any
};
export default function InputGroup(input: Input) {
	return (
		<FormControl>
			<FormLabel>{input.title}</FormLabel>
			<Input
				{...input.inputRegister}
				defaultValue={input.defaultValue}
				onKeyUp={input.callback}
				type={input.type}
				placeholder={input.placeholder}
				name={input.id ?? input.title.toLowerCase()}
				defaultValue={input.value}
			/>
			<FormHelperText>{input.message}</FormHelperText>
		</FormControl>
	);
}
