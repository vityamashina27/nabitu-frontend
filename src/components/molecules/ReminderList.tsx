"use client";
import { useEffect, useState } from "react";
import ReminderCard from "./ReminderCard";
import { useToast } from "@chakra-ui/react";
import { NextRequest } from "next/server";
import { NextApiRequest } from "next";
import cookie from "cookie";
import { accessToken, getAccessToken, tokenHandler } from "@/services/AuthService";

type Reminder = {
	id: number;
	title: string;
	description: string;
	event_at: string;
	remind_at: string;
};

export default function ReminderList({ q }: { q: string }) {
	const [reminders, setReminder] = useState<Reminder[]>([]);
	const toast = useToast();

	const fetchReminder = async () => {
		const endpoint = `${process.env.NEXT_PUBLIC_API_BASE_URL}/reminders${
			q && `?${new URLSearchParams(q)}`
		}`;
		const getReminders = await fetch(endpoint, {
			headers: {
				authorization: `Bearer ${accessToken()}`,
			},
		});

		return await getReminders.json();
	};

	useEffect(() => {
		const getReminder = async () => {
			const fetching = await fetchReminder();
			let { ok, msg, err, data } = fetching;

			let reminders = [];

			if (err === "ERR_INVALID_ACCESS_TOKEN") {
				const handler = await tokenHandler(fetchReminder())
			}else{
				{
					!ok && toast({ title: msg, status: "warning" });
				}
			}

			reminders = data?.reminders ?? [];

			setReminder(reminders);
		};
		getReminder();
	}, []);

	return (
		<div className="grid grid-cols-3 gap-4 mt-4">
			{reminders.length == 0
				? "Data reminder tidak ada"
				: reminders.map((reminder: Reminder) => {
						return (
							<ReminderCard
								key={reminder.id}
								id={reminder.id}
								title={reminder.title}
								description={reminder.description}
								event_at={reminder.event_at}
								remind_at={reminder.remind_at}
							/>
						);
				  })}
		</div>
	);
}
