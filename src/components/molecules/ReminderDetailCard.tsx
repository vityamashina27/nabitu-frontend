"use client";
import { BiTimeFive } from "@react-icons/all-files/bi/BiTimeFive";
import {
	Card,
	CardBody,
	CardFooter,
	CardHeader,
	Heading,
	Text,
	useToast,
} from "@chakra-ui/react";
import dayjs from "dayjs";
import { useEffect, useState } from "react";
import { accessToken, tokenHandler } from "@/services/AuthService";

type Reminder = {
	id: number;
	title: string;
	description: string;
	event_at?: number;
	remind_at?: number;
};

export default function ReminderDetailCard({ id }: { id: number }) {
	const [reminder, setReminder] = useState<Reminder>();
	const toast = useToast();

	const fetchReminder = async () => {
		const endpoint = `${process.env.NEXT_PUBLIC_API_BASE_URL}/reminders/${id}`;
		const getReminders = await fetch(endpoint, {
			headers: {
				authorization: `Bearer ${accessToken()}`,
			},
		});

		const { ok, msg,err, data } = await getReminders.json();

		if (err === "ERR_INVALID_ACCESS_TOKEN") {
			await tokenHandler(fetchReminder())
		}else{
			{
				!ok && toast({ title: msg, status: "warning" });
			}
		}

		setReminder(data ?? {});
	};
	useEffect(() => {
		fetchReminder();
	}, []);

	return (
		<Card className="w-1/2 mt-4">
			<CardHeader>
				<Heading size={"md"}>{reminder?.title}</Heading>
			</CardHeader>
			<CardBody>
				<Text>{reminder?.description}</Text>
			</CardBody>
			<CardFooter>
				<div>
					<div className="flex gap-2 items-center text-gray-300 text-xs">
						<BiTimeFive />
						<span>
							event at{" "}
							{dayjs
								.unix(reminder?.event_at ?? 0)
								.format("D MMMM YYYY h:mm A")}
						</span>
					</div>
					<div className="flex gap-2 items-center text-gray-300 text-xs">
						<BiTimeFive />
						<span>
							remind at{" "}
							{dayjs
								.unix(reminder?.remind_at ?? 0)
								.format("D MMMM YYYY h:mm A")}
						</span>
					</div>
				</div>
			</CardFooter>
		</Card>
	);
}
