"use client";
import { MdDelete } from "@react-icons/all-files/md/MdDelete";
import {
	Button,
	Card,
	CardBody,
	CardFooter,
	CardHeader,
	Divider,
	Heading,
	useToast,
} from "@chakra-ui/react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { accessToken, tokenHandler } from "@/services/AuthService";

type Reminder = {
	id: number;
	title: string;
	description: string;
	event_at: string;
	remind_at: string;
};

const destroyReminder = async (id: number) => {
	const getReminder = await fetch(
		`${process.env.NEXT_PUBLIC_API_BASE_URL}/reminders/${id}`,
		{
			method: "DELETE",
			headers: {
				authorization: "Bearer " + accessToken(),
			},
			cache: "no-store",
		}
	);
	return await getReminder.json();
};
export default function ReminderCard(reminder: Reminder) {
	const toast = useToast();
	const router = useRouter();

	const deleteReminder = async () => {
		const { ok, msg, err, data } = await destroyReminder(reminder.id);

		if (err === "ERR_INVALID_ACCESS_TOKEN") {
			const handler = await tokenHandler(destroyReminder(reminder.id));
		}

		{
			ok
				? toast({
						status: "success",
						title: "Reminder deleted, if page not refreshing press F5",
						onCloseComplete: () => {
							router.push('/');
						},
				  })
				: toast({
						status: "error",
						title: msg,
				  });
		}
	};

	return (
		<Card key={reminder.id}>
			<CardHeader>
				<Heading size={"md"} className="line-clamp-2 h-14">
					{reminder.title}
				</Heading>
				<Divider />
			</CardHeader>
			<CardBody className="line-clamp-4">{reminder.description}</CardBody>
			<CardFooter className="justify-between">
				<Link href={`/reminders/${reminder.id}`}>
					<Button>View Here</Button>
				</Link>
				<Button onClick={deleteReminder} colorScheme="red">
					<MdDelete />
				</Button>
			</CardFooter>
		</Card>
	);
}
