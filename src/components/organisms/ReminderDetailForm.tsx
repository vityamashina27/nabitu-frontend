"use client";
import FormReminder from "@/components/molecules/FormReminder";
import { accessToken, tokenHandler } from "@/services/AuthService";
import { useToast } from "@chakra-ui/react";
import { useEffect, useState } from "react";

type Reminder = {
	id: number;
	title: string;
	description: string;
	event_at: number;
	remind_at: number;
};
const fetchReminder = async (id: number) => {
    const getReminder = await fetch(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/reminders/${id}`,
        {
            headers: {
                authorization: "Bearer " + accessToken(),
            },
            cache: "no-store",
        }
    );
    return await getReminder.json();
};
export default function ReminderDetailForm({ id }: { id: number }) {
	const [reminder, setReminder] = useState<Reminder>();
    const toast = useToast()

	useEffect(() => {
        try {
            const getReminder = async () =>{
                const reminderData = await fetchReminder(id);
                const { ok, msg, err, data } = reminderData;

                if (err === "ERR_INVALID_ACCESS_TOKEN") {
                    await tokenHandler(fetchReminder(id))
                }else{
                    {
                        !ok && toast({ title: msg, status: "warning" });
                    }
                }

                setReminder(data);
            }
            getReminder();
        } catch (error: any) {
            toast({
                status:'error',
                title: error.message,
            })
        }
	}, [id]);

	return (
		<div className="w-1/2">
			<FormReminder type="edit" reminderData={reminder} />
		</div>
	);
}
