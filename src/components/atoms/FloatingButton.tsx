import { AiOutlinePlus } from "@react-icons/all-files/ai/AiOutlinePlus";
import {IconButton } from "@chakra-ui/react";
import Link from "next/link";

export default function FloatingButton() {
	return (
		// absolute bg-slate-600 p-4 z-10 text-white rounded-full bottom-10 right-8 hover:bg-white hover:border-2 hover:border-gray-600 hover:text-gray-700 cursor-pointer
		<Link href="/reminders/add" className="fixed bottom-10 right-8">
			<IconButton
				icon={<AiOutlinePlus />}
				variant={"solid"}
				colorScheme="teal"
				aria-label="Add"
				isRound={true}
				size={'lg'}
				fontSize={'36px'}
				fontWeight={900}
			/>
		</Link>
	);
}
