'use client'
import { destroyCookie } from "@/services/AuthService";
import { useRouter } from "next/navigation";

export default function LogoutButton() {
	const router = useRouter();
	const handleLogout = () => {
		destroyCookie();
		router.push("/login");
	};

	return (
		<h1 onClick={() => handleLogout()} className="text-xl hover:underline hover:cursor-pointer text-red-400 mr-4">
			Logout
		</h1>
	);
}
