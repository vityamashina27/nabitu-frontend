"use client"
import { Toast, useToast } from "@chakra-ui/react";

export default function TestToast({message} :{message: string}) {
    console.log('opened')
    const toast = useToast();
    toast({
        'title' : message
    });
    return <></>
};
